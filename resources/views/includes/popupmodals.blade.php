{{-- login modal --}}


<div class="modal fade" id="loginmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- alerts --}}
                    <div class="alert alert-success" role="alert">
                        You have registered sucessfuly,login please
                    </div>
                    @if(Session::has("log_email"))
                        <div class="alert alert-danger" role="alert">
                            Wrong email !
                        </div>
                    @elseif(Session::has("log_Password"))
                        <div class="alert alert-danger" role="alert">
                            Wrong Password !
                        </div>
                    @endif
                <form action="" method="get" onsubmit="logmein()">
                    {{-- Email --}}
                    @csrf
                    <div class="form-group">
                        <label  class="col-form-label">Email:</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">@</div>
                            </div>
                            <input type="text" name="email" class="form-control" id="inlineFormInputGroup" placeholder="Email">
                        </div>
                    </div>
                    {{-- password --}}
                    <div class="form-group">
                        <label  class="col-form-label" >Password:</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fa fa-key"></i></div>
                            </div>
                            <input name="password" type="text" class="form-control"  placeholder="******">
                        </div>
                    </div>

            </div>
                {{-- buttons --}}
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal" data-toggle="modal" data-target="#registermodal" ><i class="fa fa-file"></i> Register</button>
                    <button type="submit" class="btn btn-outline-success" data-dismiss="modal"><i class="fa fa-home"></i> Login</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- register model --}}


<div class="modal fade" id="registermodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Register</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" role="alert">
                   Error !
                  </div>
                <form>
                    {{-- First name --}}
                    <div class="form-group">
                        <label  class="col-form-label">First name:</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fa fa-user"></i></div>
                            </div>
                            <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="First name">
                        </div>
                    </div>
                    {{-- Last name --}}
                    <div class="form-group">
                        <label  class="col-form-label">Last name:</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fa fa-user"></i></div>
                            </div>
                            <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Last name">
                        </div>
                    </div>
                    {{-- Email --}}
                    <div class="form-group">
                        <label  class="col-form-label">Email:</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">@</div>
                            </div>
                            <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Email">
                        </div>
                    </div>
                    {{-- password --}}
                    <div class="form-group">
                        <label  class="col-form-label" >Password:</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fa fa-key"></i></div>
                            </div>
                            <input type="text" class="form-control"  placeholder="******">
                        </div>
                    </div>
                    {{-- password confirm--}}
                    <div class="form-group">
                        <label  class="col-form-label" >Confirm Password :</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fa fa-key"></i></div>
                            </div>
                            <input type="text" class="form-control"  placeholder="******">
                        </div>
                    </div>

                </form>
            </div>
            {{-- buttons --}}
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal" onclick="verify()"><i class="fa fa-arrow-right"></i> Next</button>
            </div>
        </div>
    </div>
</div>

{{-- email verification modal --}}

<div class="modal fade" id="verifymodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Verify Email</h5>
            </div>
            <div class="modal-body">

                <form>
                    {{-- Email --}}
                    <div class="form-group">
                        <label  class="col-form-label">Enter the Vrification code below or click the link in your inbox </label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fa fa-check"></i></div>
                            </div>
                            <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="code">
                        </div>
                    </div>

                </form>
                <div class="alert alert-danger" role="alert">
                    Invalid verification code !
                </div>
            </div>
            {{-- buttons --}}
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary"><i class="fa fa-rocket"></i> Verify</button>
            </div>
        </div>
    </div>
</div>


{{-- invoice list modal --}}
<div class="modal fade" id="invoice_list_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Your invoices</h5>
            </div>
            <div class="modal-body">
                <table >
                    <tr>
                        <th  class="col-md-1" >
                            invoice number
                        </th>
                        <th class="col-md-1">
                            invoice date
                        </th>
                    </tr>
                    <tr>
                        <td class="col-md-1">
                           <a href="" data-dismiss="modal" data-toggle="modal" data-target="#invoice_view_modal">45622</a>
                        </td>
                        <td class="col-md-1">
                            20-08-2020
                        </td>
                    </tr>
                </table>

            </div>
            {{-- buttons --}}
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal" ><i class="fa fa-times-circle"></i> close</button>
            </div>
        </div>
    </div>
</div>


{{-- invoice vieew modal --}}
<div class="modal fade" id="invoice_view_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Invoice 45622</h5>
            </div>
            <div class="modal-body">
                <table >
                    <tr class="m">
                        <th  class="col-md-1" >
                            Item
                        </th>
                        <th class="col-md-1">
                            Quantity
                        </th>
                        <th class="col-md-1">
                            Price
                        </th>
                    </tr>
                    <tr>
                        <td class="col-md-1">
                            book A
                        </td>
                        <td class="col-md-1">
                           2
                        </td>
                        <td class="col-md-1">
                            540
                        </td>
                    </tr>

                </table>

            </div>
            {{-- buttons --}}
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal" ><i class="fa fa-times-circle"></i> close</button>
            </div>
        </div>
    </div>
</div>





{{-- scripts --}}
<script>

// verify email load ajex
function verify(){


    var data = {"_token": "{{csrf_token()}}"}
    var request = $.ajax({
        url: "/verify",
        type: "post",
        data: data,
        dataType: "json",

        success: function (result) {


            // $("#verifymodal").modal()

            $('#verifymodal').modal('show')

        },
    });


}

// log in requst
function logmein(){


var data = {"_token": "{{csrf_token()}}"}
var request = $.ajax({
    url: "/logmein",
    type: "get",
    data: data,
    dataType: "json",

    success: function (result) {


        if(result.status=='fail'){
            console.log('ok');
            $('#loginmodal').modal('show')


        }
        else{
            
        }



    },
});


}



</script>
