<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
    <a class="navbar-brand font-weight-bold text-uppercase" href="/">Book store</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">



      </ul>

      <div class=" dropdown text-light">
        <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           My books
          </a>
          <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdown">
            <a class="dropdown-item text-light" href="#">Book A</a>
            <a class="dropdown-item text-light" href="#">Book B</a>
          </div>
      </div>
      <a class="nav-link text-light" href="#">Contact us</a>
      <a class="nav-link text-light" href="#" data-toggle="modal" data-target="#loginmodal">Register/Login</a>
      <a class="nav-link text-light" href="/profile"><img height="20" width="20" class="rounded-circle" src="images/user.png" alt=""> Profile</a>
      <a class="nav-link text-light" href="/admin_profile"><img height="20" width="20" class="rounded-circle" src="images/user.png" alt=""> Admin</a>


    </div>
  </nav>
