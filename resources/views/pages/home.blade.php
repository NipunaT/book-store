@extends('app')
@section('title', 'Home')
@section('slide')

@include('includes.slide')

@endsection
@section('content')

<div class="row justify-content-center m-5 pb-5">
    <div class="col-md-7 heading-section text-center ftco-animate">
        <span class="subheading text-success">Books section</span>
        <h2 class="mb-4">Book A</h2>
        <p class="text-muted">Far far away, behind the word mountains,</p>
    </div>
</div>
<section >
    <div class="container">
        <div class="row">
            <div class="col flex-column ">



                <p class="font-weight-light text-justify">
                    The novel is a genre of fiction, and
                    fiction may be defined as the art or craft of contriving,
                    through the written word, representations of human life that
                    instruct or divert or both. The various forms that fiction may
                    is long
                    enough to constitute a whole book, as opposed to a mere part of a book,
                    then it may be said to have achieved novelhood. But this state admits of
                    its own quantitative categories, so that a relatively brief novel may be
                    termed a novella (or, if the insubstantiality of the content matches its brevity,
                    a novelette), and a very long novel may overflow the banks of a single volume and
                    become a roman-fleuve, or river novel. Length is very much one of the dimensions of the genre.
                    <br>
                    The term novel is a truncation of the Italian word novella (from the plural of Latin
                    novellus, a late variant of novus, meaning “new”), so that what is now, in most languages,
                    a diminutive denotes historically the parent form. The novella was a kind of enlarged anecdote
                    like those to be found in the 14th-century Italian classic Boccaccio’s Decameron, each of which
                    exemplifies the etymology well enough. The stories are little new things, novelties, freshly
                    minted diversions, toys; they are not reworkings of known fables or myths, and they are lacking
                    in weight and moral earnestness..</p>
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#buynowAmodal">buy now</button>


            </div>
            <div class="col flex-column" >
            <div class="card ">
                <img  src="images/b1.jpg" class="shadow-sm" alt="">
            </div>
            </div>


        </div>
    </div>



</section>


<div class="row justify-content-center m-5 pb-5">
    <div class="col-md-7 heading-section text-center ftco-animate">
        <h2 class="mb-4">Book B</h2>
        <p class="text-muted">Far far away, behind the word mountains,</p>
    </div>
</div>
<section class=" m-5">
    <div class="container">
        <div class="row">
            <div class="col flex-column">
                <div class="card">

                    <img style="background-size: cover" class="shadow-sm" src="images/b2.jpg" alt="">
                </div>

            </div>

            <div class="col flex-column ">



                <p class="font-weight-light text-justify">
                The novel is a genre of fiction, and
                fiction may be defined as the art or craft of contriving,
                through the written word, representations of human life that
                    instruct or divert or both. The various forms that fiction may
                    take are best seen less as a number of separate categories than
                    as a continuum or, more accurately, a cline, with some such brief
                    form as the anecdote at one end of the scale and the longest
                    conceivable novel at the other. When any piece of fiction is long
                    enough to constitute a whole book,Length is very much one of the dimensions of the genre.
                <br>
                The term novel is a truncation of the Italian word novella (from the plural of Latin
                    novellus, a late variant of novus, meaning “new”), so that what is now, in most languages,
                    a diminutive denotes historically the parent form. The novella was a kind of enlarged anecdote.
                </p>
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#buynowBmodal">buy now</button>




            </div>


        </div>
    </div>



</section>

{{-- <div class="row justify-content-center m-5 pb-5">
    <div class="col-md-7 heading-section text-center ftco-animate">
        <span class="subheading text-success">About section</span>
        <h2 class="mb-4">About the Author</h2>
        <p class="text-muted">Far far away, behind the word mountains,</p>
    </div>
</div>
<section>

</section> --}}


{{-- buynow modals --}}

{{-- book A --}}
<div class="modal fade" id="buynowAmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title " id="exampleModalLabel">Book A</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <div class="card ">
                            <img  src="images/b1.jpg" class="shadow-sm" alt="">
                        </div>
                    </div>

                    <div class="col">
                        <div class="card Width 100%" >
                            <div class="card-body">
                                <table>
                                    <tr>
                                        <td class="w-50">
                                            <h5 class="card-title">Select the quantity</h5>

                                        </td>
                                        <td class="w-50">
                                            <div class="quantity">
                                                <div class="pro-qty">
                                                    <input type="text" value="0">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                          </div>
                    </div>
                </div>



            </div>
            {{-- buttons --}}
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i></i> Cancle</button>
                <button type="button" class="btn btn-outline-success"><i class="fa fa-dollar"></i> Purchase</button>
            </div>
        </div>
    </div>
</div>

{{-- book B --}}
<div class="modal fade" id="buynowBmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title " id="exampleModalLabel">Book A</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <div class="card ">
                            <img  src="images/b2.jpg" class="shadow-sm" alt="">
                        </div>
                    </div>

                    <div class="col">
                        <div class="card Width 100%" >
                            <div class="card-body">
                                <table>
                                    <tr>
                                        <td class="w-50">
                                            <h5 class="card-title">Select the quantity</h5>

                                        </td>
                                        <td class="w-50">
                                            <div class="quantity">
                                                <div class="pro-qty">
                                                    <input type="text" value="0">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                          </div>
                    </div>
                </div>



            </div>
            {{-- buttons --}}
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i></i> Cancle</button>
                <button type="button" class="btn btn-outline-success"><i class="fa fa-dollar"></i> Purchase</button>
            </div>
        </div>
    </div>
</div>



@endsection
