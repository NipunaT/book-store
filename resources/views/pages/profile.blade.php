@extends('app')
@section('title', 'Profile')
@section('content')

<div class="container">

    {{-- profile picture --}}
    <div class="d-flex justify-content-center ">
        <img class="rounded-circle mt-5" height="150" width="150" src="images/user.png" alt="">
    </div>
{{-- profile name         --}}
    <h5 class="text-uppercase font-weight-bold d-flex justify-content-center mt-2">nipuna theekshana</h5>


    <div class="row">
        <div class="col">

            <div class="card m-5">
                <div class="card-body">
                  <h5 class="card-title text-capitalize mb-5">About nipuna</h5>
                  <h6 class="card-text mb-3">Name</h6>
                  <h6 class="card-text mb-5">Email</h6>

                  <a class="card-text mb-3  text-capitalize"><i class="fa fa-shopping-cart"></i> View Shoping cart</a><br>
                  <a class="card-text mb-3  text-capitalize" data-toggle="modal" data-target="#invoice_list_modal"><i class="fa fa-money"></i> View invoices</a>

                  {{-- <a href="#" class="btn btn-primary">Go somewhere</a> --}}
                </div>
            </div>

        </div>

    </div>

</div>







@endsection
