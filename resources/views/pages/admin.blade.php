@extends('app')
@section('title', 'Admn Profile')
@section('content')

<div class="container">

    {{-- profile picture --}}
    <h5 class="text-uppercase font-weight-bold d-flex justify-content-center mt-2 text-danger">Admin profile</h5>

    <div class="d-flex justify-content-center ">
        <img class="rounded-circle mt-5" height="150" width="150" src="images/user.png" alt="">
    </div>
{{-- profile name         --}}
    <h5 class="text-uppercase font-weight-bold d-flex justify-content-center mt-2">nipuna theekshana</h5>


    <div class="row">
        <div class="col">

            <div class="card m-5">
                <div class="card-body">
                    <h5 class="card-title text-capitalize mb-5">About nipuna</h5>
                    <h6 class="card-text mb-3">Name</h6>
                    <h6 class="card-text mb-5">Email</h6>

                    <a class="card-text mb-3  text-capitalize"><i class="fa fa-list"></i> view all invoices</a><br>
                    <a class="card-text mb-3  text-capitalize" data-toggle="modal" data-target="#invoice_list_modal"><i class="fa fa-users"></i> View All users</a> <br>
                    <a class="card-text mb-3  text-capitalize" data-toggle="modal" data-target="#invoice_list_modal"><i class="fa fa-user"></i> register admin</a>

                  {{-- <a href="#" class="btn btn-primary">Go somewhere</a> --}}
                </div>
            </div>

        </div>

    </div>

{{-- dashbord --}}
    <div class="row">
        <div class="col">

            <div class="card m-5">
                <div class="card-body">
                  <h5 class="card-title text-capitalize mb-5">Dash bord</h5>
                  <a class="card-text mb-3  text-capitalize"><i class="fa fa-list"></i> Slase summary</a><br>
                  <a class="card-text mb-3  text-capitalize"><i class="fa fa-book"></i> Daily tranding book</a><br>

                  <a class="card-text mb-3  text-capitalize" data-toggle="modal" data-target="#invoice_list_modal"><i class="fa fa-flag"></i> daily salse count</a>

                  {{-- <a href="#" class="btn btn-primary">Go somewhere</a> --}}
                </div>
            </div>

        </div>

    </div>

</div>







@endsection
