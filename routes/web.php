<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});

route::post('/verify','registercontroller@verify');

route::get('/profile','profilecontroller@index');
route::get('admin_profile','profilecontroller@adminindex');

route::get('/logmein','logincontroller@log');



